package edu.upenn.cis.cis455.crawler;

import static spark.Spark.awaitInitialization;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFileLocation;
import static spark.Spark.staticFiles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegisterHandler;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class WebInterface {
    public static void main(String args[]) {
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        port(45555);
        StorageInterface database = StorageFactory.getDatabaseInstance(args[0]);

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


        before("/*", "*/*", testIfLoggedIn);
        // TODO:  add /register, /logout, /index.html, /, /lookup
//        post("/register", new RegistrationHandler(database));
        
        post("/login", new LoginHandler(database));
        
        get("/login-form.html", (request, response) -> {
        	String page = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Log in</title>\n"
        			+ "</head>\n"
        			+ "<body>\n"
        			+ "<h1>Log into Milestone 2</h1>\n"
        			+ "<p>If you don't have an account, please <a href=\"register.html\">register<a> first.</p>\n"
        			+ "\n"
        			+ "<form method=\"POST\" action=\"/login\">\n"
        			+ "Name: <input type=\"text\" name=\"username\"/><br/>\n"
        			+ "Password: <input type=\"password\" name=\"password\"/><br/>\n"
        			+ "<input type=\"submit\" value=\"Log in\"/>\n"
        			+ "</form>\n"
        			+ "\n"
        			+ "</body>\n"
        			+ "</html>";
        	response.type("text/html");
        	response.body(page);
        	return response.body();
        });
        
        post("/register", new RegisterHandler(database));
        
        get("/register.html", (request, response) -> {
        	String page = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Register account</title>\n"
        			+ "</head>\n"
        			+ "<body>\n"
        			+ "<h1>Create Account for Milestone 2</h1>\n"
        			+ "<p>Please register a user name and password</p>\n"
        			+ "\n"
        			+ "<form method=\"POST\" action=\"/register\">\n"
        			+ "Name: <input type=\"text\" name=\"username\"/><br/>\n"
        			+ "Password: <input type=\"password\" name=\"password\"/><br/>\n"
        			+ "<input type=\"submit\" value=\"Create account\"/>\n"
        			+ "</form>\n"
        			+ "\n"
        			+ "</body>\n"
        			+ "</html>";
        	response.type("text/html");
        	response.body(page);
        	return response.body();
        });
        
        get("/index.html", (request, response) -> {
//        	System.out.println("redirected to index.html");
        	String page = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
//        			+ "    <title>Welcome" + request.attribute("user") + "</title>\n"
					+ "    <title>Register account</title>\n"
        			+ "</head>\n"
        			+ "</html>";
        	response.type("text/html");
        	response.body(page);
        	return response.body();
        });
        
        
        
        
        
        

        awaitInitialization();
    }
}
