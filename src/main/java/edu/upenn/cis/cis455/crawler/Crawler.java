package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Queue;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.
	private String startUrlField;
	private StorageInterface dbField;
	private int sizeField;
	private int countField;
	private int index;
	private int crawlDelay;
	
	// need a queue of url strings to poll urls to visit from
	Queue<String> urlQueue;

    static final int NUM_WORKERS = 10;

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        startUrlField = startUrl;
        dbField = db;
        sizeField = size;
        countField = count;
        urlQueue = new LinkedList<String>();
        index = 0;
        crawlDelay = 10;
    }
    
    
 
    
    /**
     * Main thread
     */
    public void start() {
    	urlQueue.add(startUrlField);
    }
    
    
    private void checkRobot(URLInfo urlInfo, Set<String> disallowedSet) throws IOException {
    	// inspect robots.txt
		System.out.println("hostName is " + urlInfo.getHostName());
		URL robotUrl = new URL("http://" + urlInfo.getHostName() + "/robots.txt");
		HttpURLConnection conRobot = (HttpURLConnection) robotUrl.openConnection();
		conRobot.setRequestMethod("GET");
		conRobot.setRequestProperty("User-Agent", "cis455crawler");
		
		// Parse robot.txt
		InputStream inputRobot = conRobot.getInputStream();
		BufferedReader brRobot = new BufferedReader(new InputStreamReader(inputRobot));
		String tmpRobot = "";
		
		// parse
		while (tmpRobot != null) {
			tmpRobot = brRobot.readLine();
			if (tmpRobot != null && tmpRobot.startsWith("User-agent") && 
				(tmpRobot.endsWith("*") || tmpRobot.endsWith("cis455crawler"))) {
				// put all disallowed links into set
				tmpRobot = brRobot.readLine();
				while (tmpRobot != null && !tmpRobot.startsWith("User-agent")) {
					if (tmpRobot.startsWith("Disallow:")) {
						disallowedSet.add(tmpRobot.substring(10));
					} else if (tmpRobot.startsWith("Crawl-delay")) {
						crawlDelay = Integer.parseInt(tmpRobot.substring(13));
					}
					tmpRobot = brRobot.readLine();
				}
			}
		}
		brRobot.close();
    }
    
    
    private void processQueue() {
    	try {
    		System.out.println();
    		System.out.println("queue size = " + urlQueue.size());
    		// set up HttpURLConnection to read web page
    		String urlStr = urlQueue.poll();
    		System.out.println("visitng url is: " + urlStr);
    		URLInfo urlInfo = new URLInfo(urlStr);
    		
    		// inspect robot.txt
    		Set<String> disallowedSet = new HashSet<String>();
    		checkRobot(urlInfo, disallowedSet);
    		
			// stop here if the url is disallowed
			if (disallowedSet.contains(urlInfo.getFilePath())) {
				return;
			}
    		
    		
    		// first send a HEAD request to perform some checks
			URL url = new URL(urlStr);
			HttpURLConnection conHEAD = (HttpURLConnection) url.openConnection();
			conHEAD.setRequestMethod("HEAD");
			
			// test content type and length -- decide to keep or discard
			String contentType = conHEAD.getContentType();
			long contentLength = conHEAD.getContentLength();
			if (!contentType.contains("text/html") &&
				!contentType.endsWith("xml")) {
				return;
			}
			if (contentLength > 1000000 * sizeField) {
				return;
			}
			
			
			// we are sure that we can read this content type now
			// read the document and put into string
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			InputStream input = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(input));
			String page = "";
			String tmp = "";
			while (tmp != null) {
				tmp = br.readLine();
				page += tmp;
				page += "\n";
			} 
			br.close();
//			System.out.println(page);
			
			
			
			// first test if contentSeen has current page
			// if so then just ignore
			// if not we will process the whole thing and store it into contentSeen
			if (dbField.addDocument(urlStr, page) == 0) {
				return;
			}
			
			
			// now we are sure that we should index this page
			// increment index variable here?
			index++;
			
			// then if document is not html then we dont extract links
			if (!contentType.equals("text/html")) {
				return;
			}
			// use jsoup to parse the html document and retrieve links
			Document doc = Jsoup.connect(startUrlField).get();
			Elements links = doc.select("a[href]");
			// push all these links to the queue
			System.out.println("Links: " + links.size()); // test
		    for (Element link : links) {
		    	String linkURL = link.attr("abs:href");
		    	URLInfo linkUrlInfo = new URLInfo(linkURL);
		    	if (!disallowedSet.contains(linkUrlInfo.getFilePath())) {
		    		urlQueue.add(linkURL);
		    	}
		    	System.out.println(link.attr("abs:href")); // test
		    }
		    
		    
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
    }
    
    
    
    
    

    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
    	return index > countField;
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();
        
        // temp
//        crawler.processQueue();

        
        while (!crawler.isDone()) {
        	try {
        		crawler.processQueue();
                Thread.sleep(crawler.crawlDelay);
                crawler.crawlDelay = 10;
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
            

        // TODO: final shutdown
        db.close();

        System.out.println("Done crawling!");
    }

}
