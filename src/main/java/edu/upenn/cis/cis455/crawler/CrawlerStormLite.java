package edu.upenn.cis.cis455.crawler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;


public class CrawlerStormLite implements CrawlMaster{
	static Logger log = LogManager.getLogger(CrawlerStormLite.class);

	private static final String CRAWLER_QUEUE_SPOUT = "WORD_SPOUT";
    private static final String DOC_FETCHER_BOLT = "DOC_FETCHER_BOLT";
    private static final String DOM_PARSER_BOLT = "DOM_PARSER_BOLT";
    private static final String PATH_MATCHER_BOLT = "PATH_MATCHER_BOLT";
    private static final String LINK_EXTRACTOR_BOLT = "LIK_EXTRACTOR_BOLT";
    private static final String FILTER_BOLT = "FILTER_BOLT";
    
    
    
    
    
    public static void main(String[] args) throws Exception {
    	// from old crawler
    	if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);
    	
        
        
    	
    	// code from TestWordCount
        Config config = new Config();

        /*
        WordSpout spout = new WordSpout();
        WordCounter bolt = new WordCounter();
        PrintBolt printer = new PrintBolt();

        // wordSpout ==> countBolt ==> MongoInsertBolt
        TopologyBuilder builder = new TopologyBuilder();

        // Only one source ("spout") for the words
        builder.setSpout(WORD_SPOUT, spout, 1);

        // Four parallel word counters, each of which gets specific words
        builder.setBolt(COUNT_BOLT, bolt, 4).fieldsGrouping(WORD_SPOUT, new Fields("word"));

        // A single printer bolt (and officially we round-robin)
        builder.setBolt(PRINT_BOLT, printer, 4).shuffleGrouping(COUNT_BOLT);
        */
        
        
        TopologyBuilder builder = new TopologyBuilder();
        
        // TODO: create 6 new spouts/bolts, link them together
        
        
        LocalCluster cluster = new LocalCluster();
        Topology topo = builder.createTopology();

        ObjectMapper mapper = new ObjectMapper();
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        
        cluster.submitTopology("test", config, 
        		builder.createTopology()); // this line runs everything i think
        Thread.sleep(300000);
        cluster.killTopology("test");
        cluster.shutdown();
        System.exit(0);
    }





	@Override
	public void incCount() {
		// TODO Auto-generated method stub
		
	}





	@Override
	public boolean isDone() {
		// TODO Auto-generated method stub
		return false;
	}





	@Override
	public void setWorking(boolean working) {
		// TODO Auto-generated method stub
		
	}





	@Override
	public void notifyThreadExited() {
		// TODO Auto-generated method stub
		
	} 
}
