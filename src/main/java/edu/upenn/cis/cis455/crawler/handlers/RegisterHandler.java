package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.HaltException;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Session;

public class RegisterHandler implements Route {
    StorageInterface db;

    public RegisterHandler(StorageInterface db) {
        this.db = db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Register request for " + user + " and " + pass);
        if (db.addUser(user, pass) == 0) {
            System.err.println("Registered!");
        } else {
            System.err.println("User already exists!");
        }

        return "";
    }
}
