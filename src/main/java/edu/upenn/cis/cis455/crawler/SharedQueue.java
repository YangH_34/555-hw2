package edu.upenn.cis.cis455.crawler;

import java.util.LinkedList;
import java.util.Queue;

public class SharedQueue {
	//create an object of SingleObject
	private static SharedQueue instance = new SharedQueue();
	private static Queue<String> urlQueue = new LinkedList<String>();

    // private constructor so that this class cannot be instantiated
    private SharedQueue(){}
    
    
    public static synchronized SharedQueue getInstance(){
       return instance;
    }
    
    public static synchronized String popURL() {
    	return urlQueue.poll();
    }
    
    public static synchronized void addURL(String url) {
    	urlQueue.add(url);
    }

}
