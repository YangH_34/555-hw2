package edu.upenn.cis.cis455.storage;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.collections.StoredEntrySet;
import com.sleepycat.collections.StoredKeySet;
import com.sleepycat.collections.StoredSortedMap;

public class StorageViews {
	private StoredSortedMap userMap;
	private StoredKeySet contentSeenSet;
	private StoredSortedMap documentMap;
	
	public StorageViews(StorageDB db) {
		ClassCatalog catalog = db.getClassCatalog();
		EntryBinding userKeyBinding = new SerialBinding(catalog, String.class);
		EntryBinding userValueBinding = new SerialBinding(catalog, String.class);
		EntryBinding contentSeenBinding = new SerialBinding(catalog, String.class);
		EntryBinding documentKeyBinding = new SerialBinding(catalog, String.class);
		EntryBinding documentValueBinding = new SerialBinding(catalog, String.class);
		
		
		userMap = new StoredSortedMap(db.getUserDatabase(), userKeyBinding, userValueBinding, true);
		contentSeenSet = new StoredKeySet(db.getContentSeenDatabase(), contentSeenBinding, true);
		documentMap = new StoredSortedMap(db.getDocumentDatabase(), documentKeyBinding, documentValueBinding, true);
	
		contentSeenSet.clear();
//		documentMap.clear();
		System.out.println("userMap size = " + userMap.size());
		System.out.println("contentSeenSet size = " + contentSeenSet.size());
		System.out.println("documentMap size = " + documentMap.size());
	}
	
	public final StoredSortedMap getUserMap() {
		return userMap;
	}
	
//	public final StoredEntrySet getUserSet() {
//		return (StoredEntrySet) userMap.entrySet();
//	}
	
	public final StoredKeySet getContentSeenSet() {
		return contentSeenSet;
	}
	
	public final StoredSortedMap getDocumentMap() {
		return documentMap;
	}
	
//	public final StoredEntrySet getDocumentSet() {
//		return (StoredEntrySet) documentMap.entrySet();
//	}
}
