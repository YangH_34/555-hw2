package edu.upenn.cis.cis455.storage;

import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.DatabaseException;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import java.io.File;
import java.io.FileNotFoundException;

public class StorageDB {
	// fields
	private Environment env;
	private static final String CLASS_CATALOG = "java_class_catalog";
	private StoredClassCatalog javaCatalog;
	
	private static final String USER = "user";
	private Database userDb;
	
	private static final String CONTENT_SEEN = "content_seen";
	private Database contentSeenDb;
	
	private static final String DOCUMENT = "document";
	private Database documentDb;
		
	
	// constructor
	public StorageDB(String homeDirectory) 
			throws DatabaseException, FileNotFoundException {
			
		// open an environment
		System.out.println("Opening environment in: " + homeDirectory);
		EnvironmentConfig envConfig = new EnvironmentConfig();
	    envConfig.setTransactional(true);
	    envConfig.setAllowCreate(true);
	    env = new Environment(new File(homeDirectory), envConfig);
	        
	    // set up a class catalog
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, dbConfig);
        javaCatalog = new StoredClassCatalog(catalogDb);
        
        // open database for username and password -- clean when start
        userDb = env.openDatabase(null, USER, dbConfig);
        contentSeenDb = env.openDatabase(null, CONTENT_SEEN, dbConfig);
        documentDb = env.openDatabase(null, DOCUMENT, dbConfig);
	        	        
	}
		
		
	// ---------- getters -------------
	public final Environment getEnvironment() {
		return env;
	}
	
	public final StoredClassCatalog getClassCatalog() {
		return javaCatalog;
	}
	
	public final Database getUserDatabase() {
		return userDb;
	}
	
	public final Database getContentSeenDatabase() {
		return contentSeenDb;
	}
	
	public final Database getDocumentDatabase() {
		return documentDb;
	}
	
	
	// ----- other methods --------
	public void close() throws DatabaseException {
		userDb.close();
		contentSeenDb.close();
		documentDb.close();
		javaCatalog.close();
		env.close();
	}
	
}
