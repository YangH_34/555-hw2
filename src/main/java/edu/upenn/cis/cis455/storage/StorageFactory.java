package edu.upenn.cis.cis455.storage;

import java.io.FileNotFoundException;

import com.sleepycat.je.DatabaseException;

public class StorageFactory {
    public static StorageInterface getDatabaseInstance(String directory) {
        // TODO: factory object, instantiate your storage server
    	try {
			StorageControl storage = new StorageControl(directory);
			return storage;
		} catch (DatabaseException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
        return null;
    }
}
