package edu.upenn.cis.cis455.storage;

import com.sleepycat.je.DatabaseException;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Set;

import com.sleepycat.collections.TransactionRunner;
import com.sleepycat.collections.TransactionWorker;


//used example in Berkeley DB Java Edition Collections Tutorial as reference
public class StorageControl  implements StorageInterface {
	
	private StorageDB db;
	private StorageViews views;
	
	// constructor
	public StorageControl(String homeDir) 
			throws DatabaseException, FileNotFoundException {
		db = new StorageDB(homeDir);
		views = new StorageViews(db);
	}
	
	
	
	
	/*
	// ----------------- Transactions ---------------------------
	private void run() throws Exception {
		TransactionRunner runner = new TransactionRunner(db.getEnvironment());
		runner.run(new PopulateDatabase());
		runner.run(new PrintDatabase());
	}
	
	private class PopulateDatabase implements TransactionWorker {
		public void doWork() throws Exception {
			
		}
	}
	
	private class PrintDatabase implements TransactionWorker
    {
        public void doWork()
            throws Exception
        {
        }
    }
    */
	
	
	
	
	// --------------- Interface methods -------------------
	@Override
	public int getCorpusSize() {
		return 0;
	}

	@Override
	public int addDocument(String url, String documentContents) {
		try {
			Set<String> contentSeen = views.getContentSeenSet();
			String encodedDocument = encodeDoc(documentContents);
			if (contentSeen.contains(encodedDocument)) {
				return 0; // have visited
			} else {
				System.out.println("try and see if it is in document db");
				Map<String, String> documentMap = views.getDocumentMap();
				// if has old document and has no difference then do nothing
				if (documentMap.containsKey(url) && 
					documentMap.get(url).equals(encodedDocument)) {
					System.out.println("same document already exists");
					return 0;
				}
				System.out.println("inserted/replaced document");
				documentMap.put(url, encodedDocument);
				return 1;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public String getDocument(String url) {
		Map<String, String> documentMap = views.getDocumentMap();
		return documentMap.get(url);
	}

	
	@Override
	public int addUser(String username, String password) {
		// consider using transactions here -- use this for now
		try {
			Map<String, String> users = views.getUserMap();
			String encoded = encodePassword(password);
			// if user already exists, return -1; else insert and return 0;
			if (users.containsKey(username)) {
				return -1;
			} else {
				users.put(username, encoded);
				return 0;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return -1;
		}
	}
	
	// for encoding password
	private static String encodePassword(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] hash = md.digest(password.getBytes(StandardCharsets.UTF_8));
		BigInteger number = new BigInteger(1, hash);
		StringBuilder hexString = new StringBuilder(number.toString(16));
		while (hexString.length() < 32) {
			hexString.insert(0, '0');
		}
		return hexString.toString();
	}
	
	// for encoding webpage
	private static String encodeDoc(String doc) throws NoSuchAlgorithmException {
 		MessageDigest md = MessageDigest.getInstance("MD5");
 		byte[] hash = md.digest(doc.getBytes(StandardCharsets.UTF_8));
 		BigInteger number = new BigInteger(1, hash);
 		StringBuilder hexString = new StringBuilder(number.toString(16));
 		while (hexString.length() < 32) {
 			hexString.insert(0, '0');
 		}
 		return hexString.toString();
 	}
	

	// Tries to log in the user, or else throws a HaltException, 
	@Override
	public boolean getSessionForUser(String username, String password) {
		// for now just tests if user exists
		try {
			Map<String, String> users = views.getUserMap();
			String encoded = encodePassword(password);
			if (users.containsKey(username) && encoded.equals(users.get(username))) {
				return true;
			} else {
				return false;
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		}
	}

	
	@Override
	public void close() 
		throws DatabaseException {
		db.close();
	}

}
